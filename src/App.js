import React from "react";
import MainTemplate from "./Templates/MainTemplate";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./Pages/Home/Home";
import Launches from "./Pages/Launches/Launches";
import Launch from "./Pages/Launch/index";
import Rocket from "./Pages/Rocket/Rocket";
import Rockets from "./Pages/Rockets/Rockets";
import "react-image-lightbox/style.css"; // This only needs to be imported once in your app

function App() {
    return (
        <Router>
            <div>
                <MainTemplate>
                    <Switch>
                        <Route path="/launches">
                            <Launches />
                        </Route>
                        <Route path="/rockets/:rocket_id">
                            <Rocket />
                        </Route>
                        <Route path="/rockets">
                            <Rockets />
                        </Route>
                        <Route path="/launch/:flight_number">
                            <Launch />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </MainTemplate>
            </div>
        </Router>
    );
}

export default App;
