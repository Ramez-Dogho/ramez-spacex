import { makeStyles } from "@material-ui/core/styles";
export const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1
    },

    title: {
        flexGrow: 1
    },
    link: {
        color: "white",
        textDecoration: "none"
    }
}));
