import React from "react";
import PropTypes from "prop-types";
import { useStyles } from "./style";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";

import { Link } from "react-router-dom";

const MainTemplate = ({ children }) => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <AppBar position="static" className={classes.root}>
                <Container maxWidth="xl">
                    <Toolbar>
                        <Typography variant="h5" className={classes.title}>
                            <Link to="/" className={classes.link}>
                                SpaceX
                            </Link>
                        </Typography>
                        <Button color="inherit">
                            <Link to="/launches" className={classes.link}>
                                Launches
                            </Link>
                        </Button>
                        <Button color="inherit">
                            <Link to="/rockets" className={classes.link}>
                                Rockets
                            </Link>
                        </Button>
                    </Toolbar>
                </Container>
            </AppBar>
            <Container maxWidth="lg">{children}</Container>
        </React.Fragment>
    );
};
MainTemplate.propTypes = {
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
};
export default MainTemplate;
