import React from "react";
import { useStyles } from "./style";
import CircularProgress from "@material-ui/core/CircularProgress";
import PropTypes from "prop-types";
import LaunchesCard from "../LanuchesCard/index";
import Grid from "@material-ui/core/Grid";

const LaunchesList = ({ launches }) => {
    const classes = useStyles();
    return (
        <>
            <Grid container spacing={3}>
                {launches === null && (
                    <Grid
                        className={classes.spinnerContainer}
                        item
                        xl={12}
                        lg={12}
                        md={12}
                        sm={12}
                        xs={12}
                    >
                        <CircularProgress />{" "}
                    </Grid>
                )}

                {launches &&
                    launches.map((launch, index) => {
                        return (
                            <Grid
                                key={`${launch.launch_date_local}-${index}`}
                                item
                                sm={12}
                                xs={12}
                                lg={6}
                                md={6}
                                className={classes.box}
                            >
                                <LaunchesCard
                                    image={launch.links.mission_patch_small}
                                    name={launch.mission_name}
                                    year={launch.launch_year}
                                    location={launch.launch_site.site_name}
                                    rocket={launch.rocket.rocket_id}
                                    launch_success={launch.launch_success}
                                    flight_number={launch.flight_number}
                                />
                            </Grid>
                        );
                    })}
            </Grid>
        </>
    );
};
LaunchesList.propTypes = {
    launches: PropTypes.array
};
export default LaunchesList;
