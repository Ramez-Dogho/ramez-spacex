import { makeStyles } from "@material-ui/core/styles";
export const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    box: {
        textAlign: "center",
        color: theme.palette.text.secondary
    },
    h: {
        marginBottom: 10
    },
    spinnerContainer: {
        display: "flex",
        justifyContent: "center",
        height: "50vh",
        alignItems: "center"
    }
}));
