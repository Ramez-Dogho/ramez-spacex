import React from "react";
import Typography from "@material-ui/core/Typography";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";

const RocketStages = ({
    reusable,
    fuel,
    engine,
    burn,
    engine_sec,
    fuel_sec,
    burn_sec
}) => {
    const useStyles = makeStyles({
        root: {
            maxWidth: "320px"
        },

        bullet: {
            display: "inline-block",
            margin: "0 2px",
            transform: "scale(0.8)"
        },
        title: {
            fontSize: 14
        },
        pos: {
            marginBottom: 12
        },
        typo: {
            display: "flex",
            justifyContent: "space-between",
            fontSize: 18
        }
    });

    const classes = useStyles();

    const reusabilityIcon = reusable === true ? <CheckIcon /> : <ClearIcon />;

    return (
        <>
            <div className={classes.root}>
                <Typography gutterBottom variant="h4" component="h2">
                    First Stage
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span>Engine :</span> <span> {engine}</span>
                </Typography>
                <Typography className={classes.typo} variant="body1">
                    <span>Reusable :</span> <span>{reusabilityIcon}</span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span> Fuel amount tons :</span> <span>{fuel}</span>
                </Typography>
                <Typography
                    gutterBottom
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span>Burn time sec : </span> <span>{burn}</span>
                </Typography>
                <Typography gutterBottom variant="h4" component="h2">
                    Second Stage
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span>Engine :</span> <span>{engine_sec}</span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span> Fuel amount tons :</span> <span>{fuel_sec}</span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span>Burn time sec :</span> <span>{burn_sec}</span>
                </Typography>
            </div>
        </>
    );
};
RocketStages.propTypes = {
    reusable: PropTypes.bool,
    fuel: PropTypes.number,
    engine: PropTypes.number,
    burn: PropTypes.number,
    engine_sec: PropTypes.number,
    fuel_sec: PropTypes.number,
    burn_sec: PropTypes.number
};
export default RocketStages;
