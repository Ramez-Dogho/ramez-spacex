import { makeStyles } from "@material-ui/core/styles";
export const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        display: "flex",
        direction: "row",
        justifyContent: "center",
        alignItems: "center",
        height: 40
    },
    Link: {
        textDecoration: "none"
    },
    content: {
        width: "100%",
        flexDirection: "row",
        display: "flex",
        justifyContent: "space-between"
    },
    isActive: {
        color: ({ active }) => (active === true ? "green" : "red")
    }
}));
