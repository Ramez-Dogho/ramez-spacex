import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { useStyles } from "./style";
import PropTypes from "prop-types";
const RocketCard = ({ name, cost, rocketId, isActive }) => {
    const classes = useStyles({
        active: isActive
    });
    return (
        <>
            <Link to={`/rockets/${rocketId}`} className={classes.Link}>
                <Card className={classes.root}>
                    <CardContent className={classes.content}>
                        <FiberManualRecordIcon className={classes.isActive} />
                        <Typography
                            component="h5"
                            variant="h5"
                            className={classes.nme}
                        >
                            {name}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            {cost}$ per Launch
                        </Typography>
                    </CardContent>
                </Card>
            </Link>
        </>
    );
};
RocketCard.propTypes = {
    name: PropTypes.string,
    cost: PropTypes.number,
    rocketId: PropTypes.string,
    isActive: PropTypes.bool
};
export default RocketCard;
