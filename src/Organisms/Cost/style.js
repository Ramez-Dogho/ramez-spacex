import { makeStyles } from "@material-ui/core/styles";
export const useStyles = makeStyles({
    root: {
        maxWidth: "320px",
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-end"
    },

    price: {
        fontSize: 48,
        color: "#1565C0",
        fontWeight: "400",
        lineHeight: "28px",
        marginBottom: 15
    },
    perLaunch: {
        fontSize: 20,
        fontWeight: "bold"
    }
});
