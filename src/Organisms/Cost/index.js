import React from "react";
import Typography from "@material-ui/core/Typography";
import { useStyles } from "./style";
import PropTypes from "prop-types";
const Cost = ({ cost }) => {
    const classes = useStyles();
    return (
        <>
            <div className={classes.root}>
                <Typography
                    component="h5"
                    variant="h3"
                    margin="20px"
                    className={classes.price}
                >
                    {cost}$
                </Typography>
                <div className={classes.perLaunch}>per launch</div>
            </div>
        </>
    );
};
Cost.propTypes = {
    cost: PropTypes.number
};
export default Cost;
