import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Grid, InputLabel, Paper } from "@material-ui/core";
import Radio from "@material-ui/core/Radio";
import axios from "axios";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import clsx from "clsx";
import PropTypes from "prop-types";
import { useStyles } from "./style";

const FilterForm = ({ setLaunches }) => {
    const classes = useStyles();

    const SORT_OPTIONS = [
        {
            label: "Flight Number ascending",
            value: 1
        },
        {
            label: "Flight Number descending",
            value: 2
        },
        {
            label: "Mission Name ascending",
            value: 3
        },
        {
            label: "Mission Name descending",
            value: 4
        },
        {
            label: "Year ascending",
            value: 5
        },
        {
            label: "Year descending",
            value: 6
        }
    ];

    const [values, setValues] = useState({
        year: "",
        flightNumber: "",
        launchSuccess: "",
        sort: "",
        order: "",
        rocketId: ""
    });

    const reset = () => {
        setValues({
            year: "",
            flightNumber: "",
            launchSuccess: "",
            sort: "",
            order: "",
            rocketId: ""
        });
    };

    const handleChangeSort = (e) => {
        switch (e.target.value) {
            case 1:
                setValues({
                    ...values,
                    sort: "flight_number",
                    order: "asc"
                });
                break;

            case 2:
                setValues({
                    ...values,
                    sort: "flight_number",
                    order: "desc"
                });
                break;

            case 3:
                setValues({
                    ...values,
                    sort: "mission_name",
                    order: "asc"
                });
                break;
            case 4:
                setValues({
                    ...values,
                    sort: "mission_name",
                    order: "desc"
                });
                break;
            case 5:
                setValues({
                    ...values,
                    sort: "launch_year",
                    order: "asc"
                });
                break;
            case 6:
                setValues({
                    ...values,
                    sort: "launch_year",
                    order: "desc"
                });
                break;

            default:
                break;
        }
    };

    const getSortValue = () => {
        if (values.sort === "flight_number" && values.order === "asc") {
            return 1;
        } else if (values.sort === "flight_number" && values.order === "desc") {
            return 2;
        } else if (values.sort === "mission_name" && values.order === "asc") {
            return 3;
        } else if (values.sort === "mission_name" && values.order === "desc") {
            return 4;
        } else if (values.sort === "launch_year" && values.order === "asc") {
            return 5;
        } else if (values.sort === "launch_year" && values.order === "desc") {
            return 6;
        } else {
            return 1;
        }
    };

    const handleChangeYear = (event) => {
        console.log(event.target);
        const newValue = { ...values, year: event.target.value };
        setValues(newValue);
    };

    const handleChangeFlightNumber = (event) => {
        console.log(event.target);
        const newValue = { ...values, flightNumber: event.target.value };
        setValues(newValue);
    };
    const handleChangeLaunchSuccess = (event) => {
        console.log(event.target);
        const newValue = { ...values, launchSuccess: event.target.value };
        setValues(newValue);
    };
    function StyledRadio(props) {
        const classes = useStyles();

        return (
            <Radio
                className={classes.rad}
                disableRipple
                color="default"
                checkedIcon={
                    <span className={clsx(classes.icon, classes.checkedIcon)} />
                }
                icon={<span className={classes.icon} />}
                {...props}
            />
        );
    }

    const handleFileterLaunches = () => {
        setLaunches(null);
        axios
            .get(
                `https://api.spacexdata.com/v3/launches?sort=${values.sort}&order=${values.order}&launch_year=${values.year}&launch_success=${values.launchSuccess}&flight_number=${values.flightNumber}&rocket_id=${values.rocketId}`
            )
            .then((response) => {
                console.log(response.data);
                setLaunches(response.data);
            });
    };

    const [rockets, setRockets] = useState();

    useEffect(() => {
        axios.get("https://api.spacexdata.com/v3/rockets").then((response) => {
            console.log(response.data);
            setRockets(response.data);
        });
    }, []);
    const handleChange = (event) => {
        console.log(event.target);
        const newValue = { ...values, rocketId: event.target.value };
        setValues(newValue);
    };
    return (
        <>
            <form
                // onSubmit={handleFileterLaunches}
                className={classes.root}
                noValidate
                autoComplete="off"
            >
                <div className={classes.paperContainer}>
                    <Paper elevation={3} className={classes.paperArticle}>
                        <Grid>
                            <Grid className={classes.spacing} item lg={12}>
                                <TextField
                                    id="outlined-multiline-flexible"
                                    label="Year"
                                    multiline
                                    maxrows={4}
                                    value={values.year}
                                    onChange={handleChangeYear}
                                    variant="outlined"
                                    name="title" // important for event.target.name
                                />
                            </Grid>
                            <Grid className={classes.spacing} item lg={12}>
                                <TextField
                                    id="outlined-multiline-static"
                                    label="Flight Number"
                                    multiline
                                    maxrows={4}
                                    value={values.flightNumber}
                                    onChange={handleChangeFlightNumber}
                                    variant="outlined"
                                    name="content" // important for event.target.name
                                />
                            </Grid>
                            <Grid className={classes.spacing} item lg={12}>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">
                                        Launch Succes
                                    </FormLabel>
                                    <RadioGroup
                                        aria-label="gender"
                                        name="gender1"
                                        value={values.launchSuccess}
                                        onChange={handleChangeLaunchSuccess}
                                    >
                                        <FormControlLabel
                                            value="true"
                                            control={<StyledRadio />}
                                            label="success"
                                        />
                                        <FormControlLabel
                                            value="fales"
                                            control={<StyledRadio />}
                                            label="failed"
                                        />
                                        <FormControlLabel
                                            value=""
                                            control={<StyledRadio />}
                                            label="all"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                            <Grid item lg={12}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel id="select-label">
                                        Sort
                                    </InputLabel>

                                    <Select
                                        fullWidth
                                        value={getSortValue()}
                                        onChange={handleChangeSort}
                                    >
                                        {SORT_OPTIONS.map((option, index) => (
                                            <MenuItem
                                                value={option.value}
                                                key={index}
                                            >
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>

                            <Grid item lg={12}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel id="select-label">
                                        Select Rocket
                                    </InputLabel>
                                    <Select
                                        fullWidth
                                        value={values.rocketId}
                                        onChange={handleChange}
                                        className={classes.spacing}
                                    >
                                        {rockets &&
                                            rockets.map((option) => (
                                                <MenuItem
                                                    value={option.rocket_id}
                                                    key={option.rocket_id}
                                                >
                                                    {option.rocket_name}
                                                </MenuItem>
                                            ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid container>
                                <Grid
                                    item
                                    lg={6}
                                    sm={6}
                                    className={classes.butt.spacing}
                                >
                                    <Button
                                        variant="outlined"
                                        color="primary"
                                        className={classes.spacing}
                                        // href="#contained-buttons"
                                        // type="submit"
                                        onClick={reset}
                                    >
                                        Clear
                                    </Button>
                                </Grid>

                                <Grid
                                    item
                                    lg={6}
                                    sm={6}
                                    className={classes.butt.spacing}
                                >
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        className={classes.spacing}
                                        // href="#contained-buttons"
                                        // type="submit"
                                        onClick={handleFileterLaunches}
                                    >
                                        Filter
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                </div>
            </form>
        </>
    );
};
FilterForm.propTypes = {
    setLaunches: PropTypes.func
};
export default FilterForm;
