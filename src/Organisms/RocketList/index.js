import React, { useState, useEffect } from "react";
import axios from "axios";
import { useStyles } from "./style";
import Grid from "@material-ui/core/Grid";
import RocketCard from "../RocketCard/index";
const RocketList = () => {
    const [rockets, setRockets] = useState(null);

    useEffect(() => {
        axios.get("https://api.spacexdata.com/v3/rockets").then((response) => {
            console.log(response.data);
            setRockets(response.data);
        });
    }, []);

    const classes = useStyles();

    return (
        <>
            <Grid container spacing={3}>
                <Grid item lg={12}>
                    <h1 className={classes.h}>Rockets</h1>
                </Grid>

                {rockets &&
                    rockets.map((rocket) => {
                        return (
                            <Grid
                                key={rocket.rocket_id}
                                item
                                xs={12}
                                lg={12}
                                md={12}
                                sm={12}
                                className={classes.box}
                            >
                                <RocketCard
                                    name={rocket.rocket_name}
                                    cost={rocket.cost_per_launch}
                                    rocketId={rocket.rocket_id}
                                    isActive={rocket.active}
                                />
                            </Grid>
                        );
                    })}
            </Grid>
        </>
    );
};

export default RocketList;
