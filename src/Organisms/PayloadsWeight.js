import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
const PayloadsWeight = ({ weights }) => {
    const useStyles = makeStyles({
        root: {
            maxWidth: "400px"
        },

        title: {
            fontSize: 14
        },

        typo: {
            display: "flex",
            justifyContent: "space-between",
            fontSize: 18
        }
    });

    const classes = useStyles();
    return (
        <>
            <div className={classes.root}>
                <Typography gutterBottom variant="h4" component="h2">
                    Payloads weight
                </Typography>

                {weights.map((item, index) => {
                    return (
                        <Typography
                            key={item.flight_id}
                            className={classes.typo}
                            variant="body1"
                            padding="20px"
                        >
                            <span>
                                {index + 1}. {item.name} :
                            </span>{" "}
                            <span>{item.kg}</span>
                        </Typography>
                    );
                })}
            </div>
        </>
    );
};
PayloadsWeight.propTypes = {
    weights: PropTypes.array
};
export default PayloadsWeight;
