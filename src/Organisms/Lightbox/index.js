import React, { useState } from "react";
import ReactLightbox from "react-image-lightbox";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import PropTypes from "prop-types";
import { useStyles } from "./style";
const Lightbox = ({ images }) => {
    const classes = useStyles();
    const [photoIndex, setphotoIndex] = useState(0);
    const [isOpen, setisOpen] = useState(false);
    return (
        <>
            <div className={classes.list}>
                <GridList
                    cellHeight={160}
                    className={classes.gridList}
                    cols={3}
                    onClick={() => setisOpen(true)}
                >
                    {/* Open Lqightbox */}
                    {images.map((image) => (
                        <GridListTile key={image.mission_patch}>
                            <img src={image} alt="jj" />
                        </GridListTile>
                    ))}
                </GridList>

                {isOpen && (
                    <ReactLightbox
                        mainSrc={images[photoIndex]}
                        nextSrc={images[(photoIndex + 1) % images.length]}
                        prevSrc={
                            images[
                                (photoIndex + images.length - 1) % images.length
                            ]
                        }
                        onCloseRequest={() => setisOpen(false)}
                        onMovePrevRequest={() =>
                            setphotoIndex(
                                (photoIndex + images.length - 1) % images.length
                            )
                        }
                        onMoveNextRequest={() =>
                            setphotoIndex((photoIndex + 1) % images.length)
                        }
                    />
                )}
            </div>
        </>
    );
};
Lightbox.propTypes = {
    images: PropTypes.array
};

export default Lightbox;
