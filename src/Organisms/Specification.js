import React from "react";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
const Specification = ({ heigh, diameter, mass, stages, boosters }) => {
    const useStyles = makeStyles({
        root: {
            maxWidth: "320px"
        },

        title: {
            fontSize: 14
        },

        typo: {
            display: "flex",
            justifyContent: "space-between",
            fontSize: 18
        }
    });

    const classes = useStyles();
    return (
        <>
            <div className={classes.root}>
                <Typography gutterBottom variant="h4" component="h2">
                    Specification
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span>Height :</span> <span>{heigh} m</span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span> Diameter :</span> <span>{diameter} </span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span> Mass :</span> <span>{mass} kg</span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span> Stages :</span> <span>{stages}</span>
                </Typography>
                <Typography
                    className={classes.typo}
                    variant="body1"
                    padding="20px"
                >
                    <span> Boosters :</span> <span>{boosters}</span>
                </Typography>
            </div>
        </>
    );
};
Specification.propTypes = {
    heigh: PropTypes.number,
    diameter: PropTypes.number,
    mass: PropTypes.number,
    stages: PropTypes.number,
    boosters: PropTypes.number
};
export default Specification;
