import React from "react";
import { useStyles } from "./style";
import { Link } from "react-router-dom";
import Chip from "@material-ui/core/Chip";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import PropTypes from "prop-types";
const LaunchesCard = ({
    image,
    name,
    year,
    location,
    rocket,
    launch_success,
    flight_number
}) => {
    let resultStatus = launch_success ? "Success" : "Failed";

    const classes = useStyles({ success: launch_success });
    return (
        <>
            <Link className={classes.Link} to={`/launch/${flight_number}`}>
                <Card className={classes.root}>
                    <CardMedia
                        component="img"
                        image={image}
                        className={classes.img}
                    />
                    {/* <div className={classes.details}> */}
                    <CardContent className={classes.content}>
                        <Typography
                            component="h5"
                            variant="h5"
                            className={classes.nme}
                        >
                            {name}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            Year : {year}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            Location : {location}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            Rocket : {rocket}
                        </Typography>
                        <div className={classes.but}>
                            <Chip
                                size="small"
                                label={resultStatus}
                                className={classes.Chip}
                            />
                        </div>
                    </CardContent>
                    {/* </div> */}
                </Card>
            </Link>
        </>
    );
};
LaunchesCard.propTypes = {
    image: PropTypes.string,
    name: PropTypes.string,
    year: PropTypes.string,
    location: PropTypes.string,
    rocket: PropTypes.string,
    launch_success: PropTypes.bool,
    flight_number: PropTypes.number
};
export default LaunchesCard;
