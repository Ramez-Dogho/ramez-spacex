import { makeStyles } from "@material-ui/core";
export const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        display: "flex",
        direction: "row",
        justifyContent: "center",
        alignItems: "center",
        height: 200

        // marginTop: 30
    },
    Link: {
        textDecoration: "none"
    },
    content: {
        width: "100%"
    },
    img: {
        // margin: "auto",
        display: "block",
        maxWidth: "100%",
        maxHeight: "100%",
        width: 128,
        height: 128
    },
    nme: {
        // width: 333
    },
    Chip: {
        background: ({ success }) => (success === true ? "#00D053" : "#E64F01"),
        width: 89,
        height: 32,
        display: "felx",
        color: "white"
    },
    but: {
        display: "flex",
        alignItems: "end",
        justifyContent: "flex-end",
        width: "100%"
    }
}));
