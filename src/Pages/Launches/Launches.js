import React, { useState, useEffect } from "react";
import axios from "axios";
import LaunchesList from "../../Organisms/LaunchesList/index";
import FilterForm from "../../Organisms/FilterForm/index";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core";
const Launches = () => {
    const useStyles = makeStyles(() => ({
        title: {
            marginBottom: "2rem",
            marginTop: "2rem"
        }
    }));
    const classes = useStyles();

    const [launches, setLaunches] = useState(null);

    useEffect(() => {
        axios.get("https://api.spacexdata.com/v3/launches").then((response) => {
            console.log(response.data);
            setLaunches(response.data);
        });
    }, []);

    return (
        <Grid container spacing={5}>
            <Grid
                item
                xl={12}
                lg={12}
                md={12}
                sm={12}
                xs={12}
                className={classes.title}
            >
                <Typography variant="h3"> Launches</Typography>
            </Grid>
            <Grid item xl={3} lg={3} md={4} sm={5} xs={12}>
                <FilterForm setLaunches={setLaunches} />
            </Grid>
            <Grid item xl={9} lg={9} md={8} sm={7} xs={12}>
                <LaunchesList launches={launches} />
            </Grid>
        </Grid>
    );
};

export default Launches;
