import React from "react";
import RocketList from "../../Organisms/RocketList/index";

const Rockets = () => {
    return (
        <>
            <RocketList />
        </>
    );
};

export default Rockets;
