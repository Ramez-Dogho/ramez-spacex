import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import RocketStages from "../../Organisms/RocketStages";
import Specification from "../../Organisms/Specification";
import PayloadsWeight from "../../Organisms/PayloadsWeight";
import Cost from "../../Organisms/Cost/index";
const Rocket = () => {
    const useStyles = makeStyles(() => ({
        root: {
            display: "flex",

            justifyContent: "center"
        },

        Paper: {
            // height: 775,
            marginTop: 30,
            display: "flex",
            padding: 20
        },
        left: {
            display: "flex",
            justifyContent: "space-between",
            flexDirection: "column",
            paddingLeft: 20,
            marginTop: 25,
            width: "60%"
        },

        Chip: {
            background: ({ Active }) => (Active === true ? "green" : "red"),
            // height: 32,
            display: "felx",
            color: "white"
        },
        right: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            width: "40%",
            marginTop: 20
        },
        logo: {
            display: "flex",

            paddingLeft: 111,
            paddingBottom: 30
        },
        space: {
            margin: 15
        }
    }));
    let { rocket_id } = useParams();
    const [oneRocket, setOneRocket] = useState(null);
    useEffect(() => {
        if (rocket_id) {
            axios
                .get(`https://api.spacexdata.com/v3/rockets/${rocket_id}`)
                .then((response) => {
                    console.log(response.data);
                    setOneRocket(response.data);
                });
        }
    }, [rocket_id]);

    let resultStatus = oneRocket && oneRocket.active ? "Active" : "Not Active";
    const classes = useStyles({
        Active: oneRocket && oneRocket.active
    });

    return (
        <>
            {oneRocket && (
                <div className={classes.root}>
                    <Paper className={classes.Paper} elevation={3}>
                        <div className={classes.left}>
                            <Typography
                                component="h5"
                                variant="h3"
                                margin="20px"
                            >
                                Rocket Name : {oneRocket.rocket_name}
                            </Typography>
                            <Typography
                                component="h5"
                                variant="h4"
                                margin="20px"
                                className={classes.space}
                            >
                                Company : {oneRocket.company}
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                            >
                                Country : {oneRocket.country}
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                            >
                                First Flight : {oneRocket.first_flight}
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                            >
                                Success Rate : {oneRocket.success_rate_pct}%
                            </Typography>
                            <div className={`${classes.but}${classes.space}`}>
                                <Chip
                                    size="small"
                                    label={resultStatus}
                                    className={classes.Chip}
                                    margin="15px"
                                />
                            </div>
                            <Typography
                                gutterBottom
                                variant="body1"
                                padding="20px"
                                className={classes.space}
                            >
                                Description : {oneRocket.description}
                            </Typography>

                            <RocketStages
                                reusable={oneRocket.first_stage.reusable}
                                fuel={oneRocket.first_stage.fuel_amount_tons}
                                engine={oneRocket.first_stage.engines}
                                burn={oneRocket.first_stage.burn_time_sec}
                                engine_sec={oneRocket.second_stage.engines}
                                fuel_sec={
                                    oneRocket.second_stage.fuel_amount_tons
                                }
                                burn_sec={oneRocket.second_stage.burn_time_sec}
                            />
                        </div>
                        <div className={classes.right}>
                            <div>
                                <Cost cost={oneRocket.cost_per_launch} />
                            </div>
                            <div>
                                <Specification
                                    heigh={oneRocket.height.meters}
                                    diameter={oneRocket.diameter.meters}
                                    mass={oneRocket.mass.kg}
                                    stages={oneRocket.stages}
                                    boosters={oneRocket.boosters}
                                />
                            </div>
                            <div>
                                <PayloadsWeight
                                    weights={oneRocket.payload_weights}
                                />
                            </div>
                        </div>
                    </Paper>
                </div>
            )}
            ;
        </>
    );
};

export default Rocket;
