import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() => ({
    root: {
        display: "flex",

        justifyContent: "center"
    },

    Paper: {
        width: 1219,
        height: 848,
        marginTop: 25,
        display: "flex",
        padding: 20
    },
    right: {
        display: "flex",
        flexDirection: "column",
        paddingLeft: 20,

        width: "60%"
    },
    Link: {
        textDecoration: "none",
        color: "black"
    },

    Chip: {
        background: ({ success }) => (success === true ? "#00D053" : "#E64F01"),
        width: 89,
        height: 32,
        display: "felx",
        color: "white"
    },
    left: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        width: "40%"
    },
    logo: {
        display: "flex",

        paddingLeft: 111,
        paddingBottom: 30
    },
    space: {
        margin: 15
    }
}));
