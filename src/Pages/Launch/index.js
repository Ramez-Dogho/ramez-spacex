import React, { useEffect, useState } from "react";
import { useStyles } from "./style";
import Paper from "@material-ui/core/Paper";
import { useParams } from "react-router-dom";
import axios from "axios";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Lightbox from "../../Organisms/Lightbox";
import { Link } from "react-router-dom";

const Launch = () => {
    let { flight_number } = useParams();

    const [oneLaunch, setOneLaunch] = useState(null);

    useEffect(() => {
        if (flight_number) {
            axios
                .get(`https://api.spacexdata.com/v3/launches/${flight_number}`)
                .then((response) => {
                    console.log(response.data);
                    setOneLaunch(response.data);
                });
        }
    }, [flight_number]); // here is the useEffect array

    let resultStatus =
        oneLaunch && oneLaunch.launch_success ? "Success" : "Failed";
    const classes = useStyles({
        success: oneLaunch && oneLaunch.launch_success
    });
    return (
        <>
            {/* at the first iteration the oneLaunch value is null until getting the data from the API so here we put a condition to check oneLaunch if it has a value display the next code, if not just ignore it.  */}
            {oneLaunch && (
                <div className={classes.root}>
                    <Paper className={classes.Paper} elevation={3}>
                        <div className={classes.right}>
                            <Typography
                                component="h5"
                                variant="h3"
                                margin="20px"
                            >
                                Mission Name : {oneLaunch.mission_name}
                            </Typography>
                            <Link
                                to={`/rockets/${oneLaunch.rocket.rocket_id}`}
                                className={classes.Link}
                            >
                                <Typography
                                    component="h5"
                                    variant="h4"
                                    className={`${classes.nme}${classes.space}`}
                                >
                                    Rocket :{" "}
                                    {oneLaunch.rocket &&
                                        oneLaunch.rocket.rocket_name}
                                </Typography>
                            </Link>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                                className={classes.space}
                            >
                                Year : {oneLaunch.launch_year}
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                                className={classes.space}
                            >
                                Date : {oneLaunch.launch_date_local}
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                                className={classes.space}
                            >
                                Site :{" "}
                                {oneLaunch.launch_site &&
                                    oneLaunch.launch_site.site_name_long}
                            </Typography>
                            <div className={`${classes.but}${classes.space}`}>
                                <Chip
                                    size="small"
                                    label={resultStatus}
                                    className={classes.Chip}
                                    margin="15px"
                                />
                            </div>
                            <Typography
                                gutterBottom
                                variant="body1"
                                padding="20px"
                                className={classes.space}
                            >
                                Details : {oneLaunch.details}
                            </Typography>

                            <iframe
                                width="560"
                                height="315"
                                src={`https://www.youtube.com/embed/${
                                    oneLaunch.links &&
                                    oneLaunch.links.youtube_id
                                }`}
                                title="YouTube video player"
                                frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                className={classes.space}
                            ></iframe>
                        </div>
                        <div className={classes.left}>
                            <div>
                                <img
                                    src={oneLaunch.links.mission_patch}
                                    alt={oneLaunch.mission_name}
                                    width="256px"
                                    height="238px"
                                    className={classes.logo}
                                />
                            </div>

                            <Lightbox images={oneLaunch.links.flickr_images} />
                        </div>
                    </Paper>
                </div>
            )}
        </>
    );
};

export default Launch;
