import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import "fontsource-roboto";
import Typography from "@material-ui/core/Typography";

const grid = {
    sm: 12,
    xs: 12,
    md: 12,
    lg: 12,
    xl: 12
};

const Home = () => {
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            padding: "4rem 2rem"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary
        },
        img: {
            margin: "auto",
            display: "block",
            maxWidth: "100%",
            maxHeight: "100%"
        },
        wel: {
            color: "#FEFEFE"
        }
        // span: {
        //     color: "#005288",
        //     display: "block"
        // }
    }));

    const classes = useStyles();
    return (
        <>
            <div className={classes.root}>
                <Grid container spacing={2} direction="row" alignItems="center">
                    <Grid item {...grid}>
                        <Typography
                            align="center"
                            variant="h2"
                            component="h2"
                            gutterBottom
                            className={classes.wel}
                        >
                            Welcome to{" "}
                        </Typography>
                    </Grid>
                    <Grid item {...grid}>
                        <img
                            alt="space-x logo"
                            className={classes.img}
                            src="https://user-images.githubusercontent.com/4155121/28894375-5aad8bd8-77dd-11e7-83d7-a4a045b7eb83.png"
                        />
                    </Grid>
                </Grid>
            </div>
        </>
    );
};

export default Home;
